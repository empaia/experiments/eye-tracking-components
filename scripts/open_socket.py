#!/usr/bin/env python3

import socket

HOST = "0.0.0.0"
PORT = 65432


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        #s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((HOST, PORT))
        s.listen()
        try:
            while True:
                print("Accept new connection...")
                conn, addr = s.accept()
                with conn:
                    while True:
                        line = conn.recv(1024)
                        if not line:
                            break
                        line = line.decode("ascii")
                        print(line)
        except:
            print("Close socket.")
            s.shutdown(socket.SHUT_RDWR)
            s.close()


if __name__ == "__main__":
    main()
