#!/usr/bin/env python3
import json
import time

from websocket import create_connection


WEBSOCKET_URL = "ws://localhost:8000/ws"
DATA = {
    "empaia_slide_id": "xyz",
    "npp_wsi": 1,
    "npp_viewer": 2,
    "viewer_wsi_x": 3,
    "viewer_wsi_y": 4,
    "viewer_wsi_width": 5,
    "viewer_wsi_height": 6,
    "viewer_screen_x": 7,
    "viewer_screen_y": 8,
    "viewer_screen_width": 9,
    "viewer_screen_height": 10
}
COUNT = 10
SLEEP = 1

def main():
    print("Create connection.")
    ws = create_connection(WEBSOCKET_URL)
    for _ in range(COUNT):
        time.sleep(SLEEP)
        print("Send data.")
        ws.send(json.dumps(DATA))
    print("Close connection.")
    ws.close()

if __name__ == "__main__":
    main()
