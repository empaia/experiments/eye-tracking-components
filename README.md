# Eye Tracking Components

## Experiment Setup

The following components are required for the experiment setup:

* Eye Tracking Software
  * this software opens a TCP socket to receive viewer updates
  * for testing purposes use `scripts/open_socket.py` instead
* Web-To-Socket Service
  * allows the App UI (browser) to send viewer updates (JSON) to a websocket.
  * the viewer update JSON data is translated to a CSV line and passed to a TCP socket (e.g. opened by the eye tracking software)
* EMPAIA App Test Suite (EATS)
  * run EMPAIA services and web clients
* Viewer App UI
  * an App UI implementing a basic slide viewer and a slide selection panel
  * used by pathologists during the experiment
* Dummy App
  * because an App UI cannot exist without an App
  * using the official hello-world Docker image

### Setup Eye Tracking Software

In Terminal 1:

The eye tracking software should listen on a TCP port. For now, we will use a script to simulate the data flow.

```bash
cd eye-tracking-components
scripts/open_socket.py
```

This script will open a TCP socket and listen on port `65432`.

### Setup Web-To-Socket Service

In Terminal 2:

```bash
cd eye-tracking-components/web-to-socket-service
cp sample.env .env  # edit WTS_SOCKET_PORT to match Eye Tracking Software port
docker-compose up --build -d
docker-compose logs -f  # show live logs
```

### Setup App UI

In Terminal 3:

```bash
cd eye-tracking-components
cp sample.env .env
docker-compose up -d
docker-compose logs -f
```

The App UI is now running on port `4200`.

### Setup EATS

In Terminal 4:

Create a virtual environment and install EATS.

```bash
cd eye-tracking-components
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Create a `wsi-mount-points.json` file, containing your WSI folders. For example:

```
{
    "/PATH/TO/MY/WSIS": "/data"
}
```

Your folder will be available in the EATS service containers as `/data`, which is important when registering slides in an upcoming step.

Now start the EATS services. Please note, that the `--app-ui-connect-src` setting is required, to allow the App UI to connect to the Web-To-Socket Service

```
eats services up --app-ui-connect-src="*" wsi-mount-points.json
```

### Setup App

In Terminal 4 (EATS) again:

```bash
# in eye-tracking-components directory
eats apps register ./dummy-app/ead.json hello-world --app-ui-url http://host.docker.internal:4200
```

### Setup Data

In Terminal 4 (EATS) again:

Create a slide file for each slide you want to register.

```bash
# in eye-tracking-components directory
mkdir slides
```

Create a `slides/slide_a.json` file:

```json
{
    "path": "/data/CMU-1.svs",
    "id": "a969a64f-a8d0-4d74-b0c8-b03ac66093e6",
    "tissue": "SKIN",
    "stain": "H_AND_E",
    "block": "Block 1"
}
```

* All fields except for `path` are optional.
* The `path` is defined by the `/data` mounted created earlier and the folder structure inside.
* The `id` must be a UUID4. A random id will be generated during registration, if this field is undefined.
* Valid values for `tissue` and `stain` can be found in the `TAG_NAME_ID` column for the corresponding `TAG_GROUP` in the [definitions/tags/tags.csv](https://gitlab.com/empaia/integration/definitions/-/blob/main/tags/tags.csv) file.
* The `block` is an arbitrary string

```bash
eats slides register slides/slide_a.json
```

Optional: To clear all data, shut down the services with the `-v` option that deletes all EATS volumes. Afterwards start the services again.

```
eats services down -v
```

## Usage

* Open `localhost:8888/wbc2` in your browser.
* A case should have been created during the slide registration process.
* The slide should be visible in the case.
* Open a new examination.
* Add the registered Dummy App to the examination using the `+ NEW` button.
* Open the Dummy App. The App UI should be loaded.
* Viewer updates should be sent to the socket (see Terminal 1) through the Web-To-Socket Service (see Terminal 2).
* In order to retrieve correct screen coordinates use the Workbench Client v2 in browser fullscreen mode (F11)
