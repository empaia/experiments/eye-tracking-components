import socket
import logging

from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from pydantic import BaseSettings, BaseModel


DATA_LINE_PREFIX = "E;1;EMPAIA-COMM;1;;;;EMPAIA-COMM-DATA"


class Settings(BaseSettings):
    socket_host: str
    socket_port: int

    class Config:
        env_prefix = "wts_"
        env_file = ".env"


class RestrictedBaseModel(BaseModel):
    """Abstract Super-class not allowing unknown fields in the **kwargs."""

    class Config:
        extra = "forbid"


class ViewerUpdate(RestrictedBaseModel):
    empaia_slide_id: str
    npp_wsi: int
    npp_viewer: int
    viewer_wsi_x: int
    viewer_wsi_y: int
    viewer_wsi_width: int
    viewer_wsi_height: int
    viewer_screen_x: int
    viewer_screen_y: int
    viewer_screen_width: int
    viewer_screen_height: int

    def to_line(self, prefix=None):
        values = [str(val) for _, val in self.dict().items()]
        if prefix:
            values = [prefix] + values
        return ';'.join(values)+"\r\n"


class SocketHandler:
    def __init__(self):
        self._sock = None

    def _close(self):
        if self._sock is None:
            return
        try:
            logger.info("Socket: Try close.")
            self._sock.close()
        except Exception as e:
            logger.error(e)

        self._sock = None

    def sendall(self, line):
        try:
            if self._sock is None:
                logger.info("Socket: Try connect.")
                self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self._sock.connect((settings.socket_host, settings.socket_port))
            
            self._sock.sendall(line)

        except Exception as e:
            logger.error(e)
            self._close()


settings = Settings()
app = FastAPI()
logger = logging.getLogger("uvicorn")
socket_handler = SocketHandler()


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    logger.info("WebSocket: Client connected.")
    try:
        while True:
            raw_data = await websocket.receive_json()
            data = ViewerUpdate.parse_obj(raw_data)
            line = data.to_line(prefix=DATA_LINE_PREFIX)
            line = line.encode("ascii")
            socket_handler.sendall(line)

            #with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            #    s.connect((settings.socket_host, settings.socket_port))
            #    s.sendall(line)
    except WebSocketDisconnect:
        logger.info("WebSocket: Client disconnected.")
